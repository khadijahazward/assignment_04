#include <stdio.h>
int main(){
	int num;
	printf("Enter a number to view multiplication table: \n");
	scanf("%d",&num);
	for (int i = 1; i <=num ;i++){
		for (int j = 1; j <=12 ; j++){
			printf("%d * %d = %d \n",j,i,i*j);
		}
		printf("\n");
	}
	return 0;
}
