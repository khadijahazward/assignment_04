#include <stdio.h>
int main(){
	int num;
	printf("Enter a number to check the factors:\n ");
	scanf("%d", &num);
	for (int i = 1; i <= num; i++){
		if (num % i == 0){
			printf("Factor of %d is %d \n", num, i);
		}
	}
	return 0;
}
